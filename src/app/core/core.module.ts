import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CoreRoutingModule } from './core-routing.module';
import { HeaderComponent } from './header/header.component';

@NgModule({
  imports: [
    CommonModule,
    NgbModule.forRoot(),
    CoreRoutingModule,
    HttpClientModule
  ],
  exports: [
    RouterModule,
    HttpClientModule,
    HeaderComponent
  ],
  declarations: [HeaderComponent]
})
export class CoreModule { }
