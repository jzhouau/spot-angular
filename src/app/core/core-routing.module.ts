import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HeaderComponent } from './header/header.component';

const routes: Routes = [
  { path: '', component: HeaderComponent },
  { path: 'account', loadChildren: '../account/account.module#AccountModule' },
  { path: 'cart', loadChildren: '../cart/cart.module#CartModule' },
  { path: 'checkout', loadChildren: '../checkout/checkout.module#CheckoutModule' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class CoreRoutingModule { }
